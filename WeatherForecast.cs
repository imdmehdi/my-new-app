using System;

namespace my_new_app
{
    public class WeatherForecast
    {
        //yues edit fom browser
        //yues fom code
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
